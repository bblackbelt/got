package com.bblackbelt.got.view.adapter

import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bblackbelt.got.R
import com.bblackbelt.got.domain.model.NetworkState

class NetworkStateViewHolder(view: View, private val retryCallback: () -> Unit) : RecyclerView.ViewHolder(view) {

    private val progressBar = view.findViewById<ProgressBar>(R.id.progress_bar)
    private val retry = view.findViewById<Button>(R.id.retry_button)
    private val errorMsg = view.findViewById<TextView>(R.id.error_msg)

    init {
        retry.setOnClickListener {
            retryCallback()
        }
    }

    fun bind(state: NetworkState?) {
        progressBar.toVisibility(state?.isLoading() ?: false)
        retry.toVisibility(state?.isError() ?: false)
        errorMsg.toVisibility(state?.isError() ?: false)
    }
}

fun View.toVisibility(visible: Boolean) {
    visibility = when (visible) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}