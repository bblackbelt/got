package com.bblackbelt.got.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bblackbelt.data.model.House
import com.bblackbelt.got.R
import com.bblackbelt.got.domain.model.NetworkState

class HousesAdapter(private val retryCallback: () -> Unit) :
    PagedListAdapter<House, RecyclerView.ViewHolder>(POST_COMPARATOR) {

    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.house_item -> getItem(position)?.let {
                (holder as HouseViewHolder).bind(it)
            }
            R.layout.network_state_item -> (holder as NetworkStateViewHolder).bind(networkState)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.house_item -> HouseViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
            R.layout.network_state_item -> NetworkStateViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(viewType, parent, false),
                retryCallback
            )
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            R.layout.house_item
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() +
                when (hasExtraRow()) {
                    true -> 1
                    false -> 0
                }
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    companion object {
        val POST_COMPARATOR = object : DiffUtil.ItemCallback<House>() {
            override fun areContentsTheSame(oldItem: House, newItem: House): Boolean =
                oldItem == newItem

            override fun areItemsTheSame(oldItem: House, newItem: House): Boolean =
                oldItem.name == newItem.name
        }
    }
}