package com.bblackbelt.got.view.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bblackbelt.data.model.House
import com.bblackbelt.got.R

class HouseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val houseName = view.findViewById<TextView>(R.id.houseName)
    private val region = view.findViewById<TextView>(R.id.region)
    private val houseWords = view.findViewById<TextView>(R.id.houseMotto)


    fun bind(item: House) {
        houseName.text = item.name
        region.text = item.region
        houseWords.text = item.words
    }
}