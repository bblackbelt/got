package com.bblackbelt.got.view.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bblackbelt.got.domain.houses.IHousesRepository
import javax.inject.Inject

class HousesViewModel
@Inject constructor(repo: IHousesRepository): ViewModel() {
    private val pageSize = MutableLiveData<Int>()
    private val repoResult =
        Transformations.map(pageSize) { repo.getHouses(it) }

    init {
        pageSize.value = 20
    }

    val users = Transformations.switchMap(repoResult) { it.pagedList }!!
    val networkState = Transformations.switchMap(repoResult) { it.networkState }!!

    fun retry() {
        repoResult?.value?.retry?.invoke()
    }
}