package com.bblackbelt.got.view

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bblackbelt.data.model.House
import com.bblackbelt.got.R
import com.bblackbelt.got.view.adapter.HousesAdapter
import com.bblackbelt.got.view.viewmodel.HousesViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private val itemDecoration: RecyclerView.ItemDecoration by lazy {
        val margin: Int = resources.getDimension(R.dimen.margin_4).toInt()
        val lateralMargin: Int = resources.getDimension(R.dimen.margin_16).toInt()
        object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.bottom = margin
                outRect.top = margin
                outRect.left = lateralMargin
                outRect.right = lateralMargin
            }
        }
    }

    @Inject
    lateinit var mFactory: ViewModelProvider.Factory

    private val mViewModel: HousesViewModel by lazy {
        ViewModelProviders.of(this, mFactory)[HousesViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAdapter()
    }

    private fun initAdapter() {
        val adapter = HousesAdapter { mViewModel.retry() }
        houses.adapter = adapter
        houses.layoutManager = LinearLayoutManager(this)
        mViewModel.users.observe(this, Observer<PagedList<House>> {
            adapter.submitList(it)
        })
        mViewModel.networkState.observe(this, Observer {
            adapter.setNetworkState(it)
        })
        houses.addItemDecoration(itemDecoration)
    }
}
