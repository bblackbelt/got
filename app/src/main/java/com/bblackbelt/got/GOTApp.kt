package com.bblackbelt.got

import com.bblackbelt.got.di.DaggerGOTComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class GOTApp: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerGOTComponent
            .builder()
            .application(this)
            .build()

}