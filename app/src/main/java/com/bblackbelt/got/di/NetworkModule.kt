package com.bblackbelt.got.di

import com.bblackbelt.data.service.GOTApiService
import com.bblackbelt.got.BuildConfig
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val CONNECTION_TIMEOUT_SECS: Long = 20

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideGsonConverterLibrary(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Singleton
    fun providesCoroutineCallAdapter(): CoroutineCallAdapterFactory = CoroutineCallAdapterFactory()

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            okHttpClientBuilder.addInterceptor(logging)
        }

        okHttpClientBuilder
            .connectTimeout(CONNECTION_TIMEOUT_SECS, TimeUnit.SECONDS)
            .readTimeout(CONNECTION_TIMEOUT_SECS, TimeUnit.SECONDS)
            .writeTimeout(CONNECTION_TIMEOUT_SECS, TimeUnit.SECONDS)

        return okHttpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideGOTApiService(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        coroutineCallAdapterFactory: CoroutineCallAdapterFactory): GOTApiService {
        val builder = Retrofit.Builder()
            .baseUrl("https://anapioficeandfire.com/")
            .addCallAdapterFactory(coroutineCallAdapterFactory)
            .addConverterFactory(gsonConverterFactory)
        val retrofit = builder.client(okHttpClient).build()
        return retrofit.create(GOTApiService::class.java)
    }
}