package com.bblackbelt.got.di

import androidx.lifecycle.ViewModel
import com.bblackbelt.got.GOTApp
import com.bblackbelt.got.di.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AndroidSupportInjectionModule::class),
    (NetworkModule::class),
    (ContributorsModule::class),
    (BindsModule::class),
    (ViewModelModule::class),
    (HelpersModule::class)])
interface GOTComponent: AndroidInjector<GOTApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: GOTApp): Builder

        fun build(): GOTComponent
    }
}