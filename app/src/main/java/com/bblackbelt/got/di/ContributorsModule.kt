package com.bblackbelt.got.di

import com.bblackbelt.got.view.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ContributorsModule {

    @ContributesAndroidInjector
    fun injectMainActivity(): MainActivity
}