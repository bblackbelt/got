package com.bblackbelt.got.di

import android.content.Context
import com.bblackbelt.got.GOTApp
import dagger.Module
import dagger.Provides

@Module
class HelpersModule {
    @Provides
    fun provideContext(app: GOTApp): Context = app.applicationContext
}