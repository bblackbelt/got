package com.bblackbelt.got.di

import com.bblackbelt.got.domain.houses.HousesRepository
import com.bblackbelt.got.domain.houses.IHousesRepository
import dagger.Binds
import dagger.Module

@Module
abstract class BindsModule {

    @Binds
    abstract fun bindsUserDataRepository(userData: HousesRepository): IHousesRepository
}