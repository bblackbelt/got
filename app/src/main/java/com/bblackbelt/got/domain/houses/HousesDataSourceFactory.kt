package com.bblackbelt.got.domain.houses

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.bblackbelt.data.model.House
import com.bblackbelt.data.service.GOTApiService
import javax.inject.Inject

class HousesDataSourceFactory
    @Inject constructor(private val api: GOTApiService) : DataSource.Factory<Int, House>() {
    val sourceLiveData = MutableLiveData<HousesPageKeyedDataSource>()
    override fun create(): DataSource<Int, House> {
        val source = HousesPageKeyedDataSource(api)
        sourceLiveData.postValue(source)
        return source
    }
}