package com.bblackbelt.got.domain

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.bblackbelt.got.domain.model.NetworkState
import kotlinx.coroutines.*

abstract class BasePageKeyedDataSource<T> : PageKeyedDataSource<Int, T>() {

    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            it.invoke()
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, T>) {
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)
        GlobalScope.launch(Dispatchers.Main) {
            try {

                val response = loadFrom(1, params.requestedLoadSize).await()
                retry = null
                networkState.postValue(NetworkState.LOADED)
                initialLoad.postValue(NetworkState.LOADED)
                callback.onResult(response, 0, 2)

            } catch (e: Exception) {
                retry = {
                    loadInitial(params, callback)
                }
                val error = NetworkState.withError(e)
                networkState.postValue(error)
                initialLoad.postValue(error)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, T>) {
        networkState.postValue(NetworkState.LOADING)
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = loadFrom(params.key, params.requestedLoadSize).await()
                retry = null
                networkState.postValue(NetworkState.LOADED)
                initialLoad.postValue(NetworkState.LOADED)
                callback.onResult(response, params.key + 1)
                networkState.postValue(NetworkState.LOADED)

            } catch (e: java.lang.Exception) {
                retry = { loadAfter(params, callback) }
                val error = NetworkState.withError(e)
                networkState.postValue(error)
            }
        }
    }

    abstract fun loadFrom(page: Int, pageSize: Int): Deferred<List<T>>

}