package com.bblackbelt.got.domain.houses

import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.bblackbelt.data.model.House
import com.bblackbelt.got.domain.model.PageDataWrapper
import javax.inject.Inject

class HousesRepository @Inject constructor(private val factory: HousesDataSourceFactory) : IHousesRepository {
    override fun getHouses(pageSize: Int): PageDataWrapper<House> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(5)
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .build()

        return PageDataWrapper(
            LivePagedListBuilder(factory, config)
                .build(),
            networkState = Transformations.switchMap(factory.sourceLiveData) {
                it.networkState
            },
            retry = {
                factory.sourceLiveData.value?.retryAllFailed()
            },
            refresh = {
                factory.sourceLiveData.value?.invalidate()
            },
            refreshState = Transformations.switchMap(factory.sourceLiveData) {
                it.initialLoad
            }
        )
    }

}