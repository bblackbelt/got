package com.bblackbelt.got.domain.houses

import com.bblackbelt.data.model.House
import com.bblackbelt.got.domain.model.PageDataWrapper

interface IHousesRepository {
    fun getHouses(pageSize: Int): PageDataWrapper<House>
}