package com.bblackbelt.got.domain.houses

import com.bblackbelt.data.model.House
import com.bblackbelt.data.service.GOTApiService
import com.bblackbelt.got.domain.BasePageKeyedDataSource
import kotlinx.coroutines.Deferred

class HousesPageKeyedDataSource(private val gotApiService: GOTApiService) : BasePageKeyedDataSource<House>() {
    override fun loadFrom(page: Int, pageSize: Int): Deferred<List<House>> = gotApiService.getHouses(page, pageSize)
}