package com.bblackbelt.data.service

import com.bblackbelt.data.model.House
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface GOTApiService {
    @GET("api/houses")
    fun getHouses(@Query("page") page: Int = 1, @Query("pageSize") pageSize: Int = 20): Deferred<List<House>>
}